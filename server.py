#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import socketserver
import sys
import simplertp
import random


class EchoHandler(socketserver.DatagramRequestHandler):
    """
    UDP echo handler class
    """
    dicc = {}

    def handle(self):
        print("Petición recibida")

        #Lectura cada línea de lo que envía el cliente
        msg = self.rfile.read().decode('utf-8')
        sip = msg.split()[2] 
        metodos = msg.split()[0]
        USER_O = 'batman@gotham.com'
        ip = sys.argv[1]
        port = int(sys.argv[2])
        audio_file = sys.argv[3]
        sdp = "v = 0\r\n" + "o = " + USER_O + ' ' + ip + "\r\n" + "s = misesion\r\n" + "t = 0\r\n" + "m = audio " + str(port) + " RTP\r\n"



        if metodos.upper() == 'INVITE' or 'ACK' or 'BYE':
            if sip != 'SIP/2.0':
                self.wfile.write(b"SIP/2.0 400 Bad Request") #Si la posición 2 de lo que envia el cliente no corresponde a "SIP/2.0" 

            if sip == 'SIP/2.0': #Si corresponde a "SIP/2.0"
                if metodos.upper() == 'INVITE':
                    self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n" + sdp.encode('utf-8')) #Si el método es 'INVITE' escribe la respuesta con body
                    #Rellena el diccionario con los datos del cliente
                    u = msg.split('\r\n')[5].split()[0].split('=')[1]
                    u_ip = msg.split('\r\n')[5].split()[1]
                    u_port = int(msg.split('\r\n')[8].split()[1])
                    self.dicc[u] = [u_ip, u_port]

                elif metodos.upper() == 'ACK': #Si el método es 'ACK' al previamente haber mandado satisfactoriamente 'INVITE', se implementa el envío del audio (Error de decodificación)
                    RTP_header = simplertp.RtpHeader()
                    RTP_header.set_header(pad_flag=0, ext_flag=0, cc=0, ssrc=random.randrange(10000))
                    audio = simplertp.RtpPayloadMp3(audio_file)
                    simplertp.send_rtp_packet(RTP_header, audio, ip, port)

  
                elif metodos.upper() == 'BYE': #Si el método es 'BYE' respuesta
                    self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n")
        else:
             self.wfile.write(b"SIP/2.0 405 Method Not Allowed\r\n\r\n") #Si el método no es ni 'INVITE' ni 'ACK' ni 'BYE'

        

def main():
    # Creamos servidor de eco y escuchamos
    try:
       IP = sys.argv[1]
       PORT = int(sys.argv[2])
       audio_file = sys.argv[3]
    except IndexError:
        sys.exit("Usage: python3 server.py <IP> <port> <audio_file>")
        
    try:
        serv = socketserver.UDPServer((IP, PORT), EchoHandler)
        print("Listening...")
    except OSError as e:
        sys.exit(f"Error empezando a escuchar: {e.args[1]}.")

    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
        sys.exit(0)
        


if __name__ == "__main__":
    main()
