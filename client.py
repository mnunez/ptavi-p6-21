#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Programa cliente que abre un socket a un servidor
"""

import socket
import sys

# Cliente UDP simple.


def main():
    # Dirección IP y puerto del servidor.
    try:
        SERVER = 'localhost'
        METODO = sys.argv[1]
        direccion = sys.argv[2].split('@')
        IP = direccion[1].split(':')[0]
        IP_O = '127.0.0.1'
        USER_O = 'robin@gotham.com'
        PORT = int(sys.argv[2].split(':')[1])
        RECEPTOR = sys.argv[2].split(':')[0]
    except IndexError:
        sys.exit("Usage: python3 client_echo.py <method> <receiver>@<IP>:<SIPport>")

    #Contenido a enviar
    LINE = METODO.upper() + ' sip:' + RECEPTOR + ' SIP/2.0\r\n'
    LINE2 = 'ACK' + ' sip:' + USER_O + ' SIP/2.0\r\n'
    # Creamos el socket y lo configuramos
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
        my_socket.connect((SERVER, PORT))

        
        if METODO.upper() == 'BYE':
            my_socket.send(bytes(LINE + '\r\n', 'utf-8'))
            print("Terminando programa...")
          
        if METODO.upper() == 'INVITE': #Body del método INVITE
            tamaño_cuerpo = sys.getsizeof("v = 0" + '\r\n' + "o = " + USER_O + " " + IP_O + '\r\n' + "s = misesion" + '\r\n' + "t = 0" '\r\n' + "m = audio " + str(PORT) + " RTP + '\r\n'")

            print("Enviando: " + LINE) 
            print()
            print("Content-Type: application/sdp\r\n")
            print("Content-Length: " + str(tamaño_cuerpo) + '\r\n')
            print("v = 0\r\n")
            print("o = " + USER_O + " " + IP_O + '\r\n')
            print("s = misesion\r\n")
            print("t = 0\r\n")
            print("m = audio " + str(PORT) + " RTP\r\n")

            my_socket.send(bytes(LINE + '\r\n', 'utf-8') + bytes("Content-Type: application/sdp\r\n", 'utf-8') + bytes("Content-Length: " + str(tamaño_cuerpo) + '\r\n', 'utf-8') + bytes("v = 0\r\n", 'utf-8') + bytes("o = " + USER_O + " " + IP_O + "\r\n", 'utf-8') + bytes("s = misesion\r\n", 'utf-8') + bytes("t = 0\r\n", 'utf-8') + bytes("m = audio " + str(PORT) + " RTP\r\n", 'utf-8') + b'\r\n\r\n')
            
            if METODO.upper() == 'INVITE':
                my_socket.send(bytes(LINE2 + '\r\n', 'utf-8')) #Envío de método 'ACK' si el envío del método 'INVITE' es satisfactorio

        data = my_socket.recv(1024)
        print("Recibido:")
        print(data.decode('utf-8') + ".")

        
        

if __name__ == "__main__":
    main()
